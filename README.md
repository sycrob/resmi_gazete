Resmi Gazete Twitter Script

Her gün Resmi Gazete Sayfasını tarayıp bağlantsını tweet atan basit bir Javascript.

Script AWS Lambda üzerinde. Her gün saat 10:00'da kendini tetikliyor. Twitter'ın açık olup olmadığına bakıyor. 
Twitter açık ise resmi gazete yaymlanmış mı kontrol ediyor. Yaymlanmış ise ilgili linki tweet atyor. 
