"use strict";
const Twitter = require("twitter");
const request = require("request-promise");
const cheerio = require("cheerio");

const twoDigit = e => 10 > e ? "0" + e : e;
const monthNames = [
	'',
	'Ocak',
	'Şubat',
	'Mart',
	'Nisan',
	'Mayıs',
	'Haziran',
	'Temmuz',
	'Ağustos',
	'Eylül',
	'Ekim',
	'Kasım',
	'Aralık',
];
const client = new Twitter({
	consumer_key: process.env.TWITTER_CONSUMER_KEY,
	consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
	access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
	access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
});

exports.handler = async (event, context) => {
	const test = event.test;
	const date = new Date();
	const year = date.getUTCFullYear();
	const month = twoDigit(date.getUTCMonth() + 1);
	const day = twoDigit(date.getUTCDate());
	
	const link = `http://www.resmigazete.gov.tr/eskiler/${year}/${month}/${year}${month}${day}`;
	let text = "tweet_sent";
	try {
		const twitter = await request({
			method: 'HEAD',
			uri: 'http://www.twitter.com'
		});
		if (!twitter) throw 'twitter down';
		const pdf = await request({
			method: 'HEAD',
			uri: link + '.pdf',
			simple: false,
			resolveWithFullResponse: true,
		});
		if (pdf.statusCode == 200) {
			const $ = await request({
				uri: link + '.htm',
				transform: body => cheerio.load(body),
			});
			if (!$) throw 'error';
			let sayi = $('p[dir="ltr"]').first().text().split(" ");

			//Gazeteye girilen metin zaman zaman değişiyor. Onunla ilgili düzeltme.
			sayi = isNaN(sayi[5]) ? sayi[6] : sayi[5];
			if(test) {
				text= `${date.getUTCDate()} ${monthNames[date.getUTCMonth()+1]} ${year} Tarihli ve ${sayi} Sayılı Resmî Gazete ${link}.htm #resmigazete`;
			} else {
				await	client.post('statuses/update', {
					status: `${date.getUTCDate()} ${monthNames[date.getUTCMonth()+1]} ${year} Tarihli ve ${sayi} Sayılı Resmî Gazete ${link}.htm #resmigazete`
				});
			}
		} else {
			if(test) {
				text = `${date.getUTCDate()} ${monthNames[date.getUTCMonth()+1]} ${year} Resmî Gazete Yayımlanmamıştır.`;
			} else {
				await	client.post('statuses/update', {
					status: `${date.getUTCDate()} ${monthNames[date.getUTCMonth()+1]} ${year} Resmî Gazete Yayımlanmamıştır.`
				});
			}
		}
	} catch (error) {
		console.log(error);
	} finally {
		return text;
	}
};